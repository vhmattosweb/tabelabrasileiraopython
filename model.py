from abc import ABC, abstractmethod


class InterfaceModel(ABC):
    @abstractmethod
    def inserir_resultado_partida(self, nome_t1, nome_t2, gols_t1, gols_t2):
        pass

    @abstractmethod
    def retornar_tabela(self):
        pass
    
class Brasileirao(InterfaceModel):
    
    __instancia = None

    __tabela_campeonato = dict()

    def __new__(cls):
        if cls.__instancia is None:
            cls.__instancia = super().__new__(cls)
        return cls.__instancia
    

    def inserir_resultado_partida(self, nome_t1, nome_t2, gols_t1, gols_t2):
        # verifica se os times estão na tabela 
        self.checa_time(nome_time=nome_t1)
        self.checa_time(nome_time=nome_t2)
        # atualiza resultado para cada time
        self.atualizar_time(nome_t1, gols_t1, gols_t2)
        self.atualizar_time(nome_t2, gols_t2, gols_t1)

    
    @classmethod
    def checa_time(cls, nome_time):
        if nome_time not in cls.__tabela_campeonato:
            cls.__tabela_campeonato[nome_time] = {
                "P": 0, "J": 0, "V": 0, "E": 0, "D": 0, "GP": 0, "GC": 0, "SG": 0
            }

    @classmethod
    def atualizar_time(cls, nome_time, gols_pro, gols_contra):

        cls.__tabela_campeonato[nome_time]["J"] += 1
        cls.__tabela_campeonato[nome_time]["GP"] += gols_pro
        cls.__tabela_campeonato[nome_time]["GC"] += gols_contra
        cls.__tabela_campeonato[nome_time]["SG"] += gols_pro - gols_contra

        if gols_pro > gols_contra:
            pontos, vitoria, empate, derrota = 3, 1, 0, 0
        
        elif gols_contra > gols_pro:
            pontos, vitoria, empate, derrota = 0, 0, 0, 1

        else:
            pontos, vitoria, empate, derrota = 1, 0, 1, 0
        
        for attr, valor in zip(["P", "V", "E", "D"], [pontos, vitoria, empate, derrota]):
            cls.__tabela_campeonato[nome_time][attr] += valor
        

    def retornar_tabela(self):
        return sorted(self.__tabela_campeonato, lambda time: time["P"], reverse=True)
    
    

