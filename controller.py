from abc import abstractmethod, ABC
from model import InterfaceModel, Brasileirao


class Controller(ABC):

    def __init__(self, modelo: InterfaceModel) -> None:
        self._model = modelo

    @abstractmethod
    def ler_resultados(self):
        pass


class ControllerTerminal(Controller):

    def ler_resultados(self):
        # loop sobre todas iterações
        while True:
            nome_time_1 = input("Primeiro time:")
            gols_time_1 = int(input(f"Gols do {nome_time_1}:"))
            nome_time_2 = input("Segundo time:")
            gols_time_2 = int(input(f"Gols do {nome_time_2}:"))
            # atualizando o modelo
            self._model.inserir_resultado_partida(nome_time_1, nome_time_2, gols_time_1, gols_time_2)
            parar_iteracao = input("Pressione enter para inserir mais partidas ou digite exit para sair:")
            if parar_iteracao == "exit":
                break


if __name__ == "__main__":

    controlador = ControllerTerminal(Brasileirao())

    controlador.ler_resultados()
