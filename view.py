from model import InterfaceModel


class View:

    def __init__(self, view_strategy):
        self.__metodo_visualizacao = view_strategy

    
    def mostrar_resultados():
        self.__metodo_visualizacao.show()


class MostrarTabela:

    def __init__(self, modelo: InterfaceModel) -> None:
        self.__modelo = modelo

    def show(self):
        for nome_time, resultados in self.__modelo.retornar_tabela().items():
            print(f"{nome_time}\t{resultados['P']}\t{resultados['P']}")